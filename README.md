# JavaScript Multiplayer Game with Websockets

Three players trying to paint all the board in their own color

## Features

- Tech stack: HTML5, CCS3 (Tailwind CSS), JavaScript, WebSockets
- Create new game
- Join existing game by game id
- Maximum 3 player in a game
- Dynamic balls amount

## Requirements

- Git
- Gitlab account
- Node.js v16.x

## Installation

- Fork [repository](https://gitlab.com/epam-ehu/js-websocket-cells-game) from gitlab web app (Fork button in the top right corner)
- Clone forked repository `git clone git@gitlab.com:your-account-name/js-websocket-cells-game.git`
- Install packages by running `npm install`
- Run project `npm run serve`
- Open `http://localhost:9091` in browser

## Materials

- Websocket explanation and code examples: [Youtube: Hussein Nasser](https://youtu.be/cXxEiWudIUY)
- Classic Front End MVC with Vanilla Javascript: [Meduim: Patrick Ackerman](https://medium.com/@patrickackerman/classic-front-end-mvc-with-vanilla-javascript-7eee550bc702)
